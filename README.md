Hello, Ahyar here!
(^o^)/

---------------------------------------
System Details : 
1. Kubernetes Cluster using Minikube
2. Kustomize
3. AWS EC2
4. Docker
5. Terraform
6. Overlay Jenkins Chart Helm (dev, prod)
7. Pre Commit (end-of-file-fixed, check-yaml)
8. SOPS
9. Namespaces : dev-jenkins, prod-jenkins

---------------------------------------
Pre-requisites : 
1. We will need AWS account to finish this steps since we will build our infra on top of the AWS EC2 Instance. Please do <aws configure> first to set our credentials.
2. Internet connection.
3. PC/Laptops
4. Patience.. kidding, we will use fast connection, and processing benefit since we are using AWS EC2 instead of localhost.
5. AWS Security Group which have all traffic inbound  
6. AWS Key-Pair to access EC2 from localhost 

---------------------------------------
Please follow below steps to start our great things here!^^

1. Take a deep breath, then open your local linux terminal
2. Enter these command to get our codes: 
    > git clone https://gitlab.com/ahyar71/jenkins-kube.git

    > cd jenkins-kube

    > git checkout master

3. Open folder aws-terraform-jenkins from our favourite Code Editor to build our architecture on AWS, then run command :
    > terraform init
4. Please adjust the main.tf on part :
    > <resource "aws_instance" "tes-jenkins"> 

    >    --> key_name : the name of AWS Key-Pair on prereq 6.

    > <resource "aws_network_interface_sg_attachment" "sg_attachment">

    >    --> security_group_id : the security group id on prereq 5
    
    > ![image](/uploads/e7ebf50dcad34ce603b5dcdb68acbf52/image.png)
5. Now we are ready to build our infra, by running this command:
    > terraform apply --auto-approve
6. Open linux terminal then, run SSH command to access our EC2 instances we have made from previous step, by running these command : 
    > cd /path/to/aws-key-pair
    
    > ssh -i <aws-key-pair>.pem ubuntu@<output instance_public_dns from step 5>
7. Congrats, We are now inside our EC2 instance.
8. Now open main.tf, then copy all user_data part then paste into terminal in step 6.
9. Wait until it finished (if you got prompt like below image <Daemons using outdated libraries> please click ESC button on your keyboard to exit)
    > ![image](/uploads/123ac3ba30a76ab17eeab0a7e9142708/image.png)
10. When it finished we already got our Kubernetes Cluster ready alongside with the Customized Jenkins Helm with Kustomize, and all of the apps prerequisites (included precommit, sops, etc).
9. If you want to check pods on preprod namespaces, We can run these command:
    > kubectl get pods -n dev-jenkins
10. Finished!


Muhammad Ahyar Arsul
Beloved DevOps Engineer

Notes : <br/>
Honestly i got an error when creating encrypted secret using sops, i have tried many others encryption kustomization plugins for 2 days but the results are the same, i got the same error depicted in this image below. From Ksops, sops-secrets-generator, kustomize-age, kustomize-sops, kustomize-sopssecretgenerator, no idea what cause this error while i already follow the instructions from many sources. This error lead to infinite init status on the pods since the secret not exists. But when we are using unencrypted secret, by comment the secret-generator.yaml and uncomment secret.yaml on base/kustomization.yaml its working. Sorry for not accomplishing this one. Hope i could get this issue solve on next occassion. Thanks!<br>

![image](/uploads/2cd1701d6fccf989264a0c65a055bd7c/image.png)

<br>
