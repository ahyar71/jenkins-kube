output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.tes-jenkins.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.tes-jenkins.public_ip
}

output "instance_public_dns" {
  description = "Private DNS of the EC2 instance"
  value       = aws_instance.tes-jenkins.public_dns
}
