terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}


provider "aws" {
  profile = "default"
  region  = "us-west-1"
}

resource "aws_security_group" "sg" {
  tags = {
    type = "tesjenkins-sg"
  }
}


resource "aws_instance" "tes-jenkins" {
  ami           = "ami-085284d24fe829cd0"
  # ami           = "ami-0e4d9ed95865f3b40"
  instance_type = "t3a.medium"
  key_name   = "tesjenkins-key"

  root_block_device {
    volume_size = 30 # in GB <<----- I increased this!
    volume_type = "gp3"
    encrypted   = true
  }

  user_data = <<-EOF

    ### COPY FROM HERE ###

    sudo apt update -y
    sudo apt install docker.io -y
    sudo usermod -aG docker $USER
    sudo usermod -aG root $USER
    sudo chmod 777 /var/run/docker.sock

    # Install kubectl
    curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    kubectl version --client

    


    # Install Minikube
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    sudo install minikube-linux-amd64 /usr/local/bin/minikube
    
    minikube start

    # Install Helm
    curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
    sudo apt-get install apt-transport-https --yes
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm

    # Install precommit
    sudo apt install pre-commit --yes

    # Install GO
    sudo apt install golang --yes
    echo 'export GOPATH=~/go' >> ~/.bashrc
    source ~/.bashrc
    mkdir $GOPATH

    # Install brew
    sudo apt-get update
    sudo apt-get -y install build-essential cmake sparsehash valgrind libibnetdisc-dev gsl-bin \
    libgsl0-dev libgsl0ldbl subversion tmux git curl parallel gcc make g++ zlib1g-dev \
    libncurses5-dev python-dev unzip dh-autoreconf default-jre python-pip \
    mcl libhdf5-dev r-base pkg-config libpng12-dev libfreetype6-dev libsm6 \
    libxrender1 libfontconfig1 liburi-escape-xs-perl liburi-perl ruby \
    python-pandas python-scipy python-numpy
    sudo mkdir /home/linuxbrew
    sudo chown $USER:$USER /home/linuxbrew
    git clone https://github.com/Linuxbrew/brew.git /home/linuxbrew/.linuxbrew
    echo 'export PATH="/home/linuxbrew/.linuxbrew/bin:$PATH"' >> ~/.profile
    echo 'export MANPATH="/home/linuxbrew/.linuxbrew/share/man:$MANPATH"' >> ~/.profile
    echo 'export INFOPATH="/home/linuxbrew/.linuxbrew/share/info:$INFOPATH"' >> ~/.profile
    source ~/.profile
    brew tap homebrew/science
    brew update
    brew doctor

    # Install SOPS and age as encryption key
    brew install sops
    brew install age


    # Install Kustomize
    curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"  | bash
    sudo install -o root -g root -m 0755 kustomize /usr/local/bin/kustomize

    # Install KSOPS
    echo "export XDG_CONFIG_HOME=\$HOME/.config" >> $HOME/.zshrc
    source $HOME/.zshrc
    source <(curl -s https://raw.githubusercontent.com/viaduct-ai/kustomize-sops/master/scripts/install-ksops-archive.sh)

    # Prepare the env  
    mkdir -p ~/minikube-helm-jenkins/minikube
    cd ~/minikube-helm-jenkins

    # helm repo add jenkins https://charts.jenkins.io
    # helm repo update
    git clone https://gitlab.com/ahyar71/jenkins-kube.git
    cd jenkins-kube
    git checkout master


    # Run precommit against .pre-commit-config.yaml
    pre-commit install
    pre-commit run --all-files

    cd miraclekustom/base/

    sops --encrypt --age age1yt3tfqlfrwdwx0z0ynwplcr6qxcxfaqycuprpmy89nr83ltx74tqdpszlw secret.yaml > secret.enc.yaml

    cd ..

    kustomize build --enable-alpha-plugins . | kubectl apply -f -


    ### UNTIL HERE ###



  EOF
  tags = {
    Name = "tes-jenkins"
  }
}

resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = "sg-011aba5921ba05d89"
  network_interface_id = aws_instance.tes-jenkins.primary_network_interface_id
}
